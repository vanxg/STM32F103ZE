#include "delay.h"

#define LED0 PBout(3)
#define LED1 PBout(4)


int main(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOE,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOE,&GPIO_InitStructure);
	GPIO_SetBits(GPIOE,GPIO_Pin_5);
	GPIO_SetBits(GPIOE,GPIO_Pin_6);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	GPIO_SetBits(GPIOB,GPIO_Pin_3);
	GPIO_SetBits(GPIOE,GPIO_Pin_4);

	while(1)
	{
		if(GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_5) == 0)
		{
		    delay_ms(200);
			if(GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_5) == 1)
			LED0 = !LED0;
		}
		else if(GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_6) == 0)
		{
		    delay_ms(200);
			if(GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_6) == 1)
			LED1 = !LED1;
		}
	}
}